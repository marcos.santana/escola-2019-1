package br.ucsal.bes20191.testequalidade.escola.business;

import br.ucsal.bes20191.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class AlunoBOUnitarioTest {

    private AlunoBO alunoBO;
    private AlunoDAO alunoDAOMock;
    private AlunoBuilder alunoBuilder;
    private DateHelper dateUtil;

    @Before
    public void setup() {
        alunoDAOMock = Mockito.mock(AlunoDAO.class);
        alunoBuilder = new AlunoBuilder();
        dateUtil = new DateHelper();

        Mockito.when(alunoDAOMock.encontrarPorMatricula(1)).thenReturn(alunoBuilder.buildAluno1());
        Mockito.when(alunoDAOMock.encontrarPorMatricula(2)).thenReturn(alunoBuilder.buildAluno2());

        alunoBO = new AlunoBO(alunoDAOMock, dateUtil);
    }

    /**
     * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
     * 16 anos.
     */
    @Test
    public void testarCalculoIdadeAluno1() {
        Integer matricula = 1;
        Integer expected = 16;

        Integer actual = alunoBO.calcularIdade(matricula);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Verificar se alunos ativos s�o atualizados.
     */
    @Test
    public void testarAtualizacaoAlunosAtivos() {
        Aluno aluno = alunoDAOMock.encontrarPorMatricula(2);

        alunoBO.atualizar(aluno);
        Mockito.verify(alunoDAOMock).salvar(aluno);
    }

}
