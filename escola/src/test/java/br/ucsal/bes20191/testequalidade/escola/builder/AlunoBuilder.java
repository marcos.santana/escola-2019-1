package br.ucsal.bes20191.testequalidade.escola.builder;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;


public class AlunoBuilder {


    Aluno aluno;

    public Aluno buildAluno1() {
        aluno = new Aluno();
        aluno.setMatricula(1);
        aluno.setAnoNascimento(2003);
        aluno.setNome("Aluno de Jesus");
        aluno.setSituacao(SituacaoAluno.CANCELADO);
        return aluno;
    }

    public Aluno buildAluno2() {
        aluno = new Aluno();
        aluno.setMatricula(2);
        aluno.setAnoNascimento(2002);
        aluno.setNome("Aluno Ativo");
        aluno.setSituacao(SituacaoAluno.ATIVO);
        return aluno;
    }

}
