package br.ucsal.bes20191.testequalidade.escola.tui;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class TuiHelperUnitarioTest {

	private TuiUtil tui;


	@Before
	public void setup(){
		tui = new TuiUtil();
	}
	/**
	 * Verificar a obtenção do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {

		ByteArrayInputStream in = new ByteArrayInputStream("Claudio\nNeiva\n".getBytes());
		System.setIn(in);
		tui.scanner = new Scanner(System.in);
		String actual = tui.obterNomeCompleto();
		String expected = "Claudio Neiva";
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Verificar a obtenção exibição de mensagem. Caso de teste: mensagem "Tem que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));

		String mensagem = "Tem que estudar.";
		String expect = "Bom dia! Tem que estudar.\n";
		tui.exibirMensagem(mensagem);
		String actual = out.toString();
		Assert.assertEquals(expect, actual);
	}

}
